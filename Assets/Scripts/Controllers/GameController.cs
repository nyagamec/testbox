using UnityEngine;

public class GameController : MonoBehaviour {
    [SerializeField] SpawnController spawnController;
    [SerializeField] UpdateController updateController;
    [SerializeField] UIController uiController;

    private void Awake() {
        updateController.Add(spawnController);

        spawnController.SpawnBoxEvent.AddListener((Box box) => updateController.Add(box));
        spawnController.DestroyBoxEvent.AddListener((Box box) => updateController.Remove(box));
        uiController.IntervalUpdateEvent.AddListener((float interval) => spawnController.SpawnInterval = interval);
        uiController.SpeedUpdateEvent.AddListener((float speed) => spawnController.Speed = speed);
        uiController.DistanceUpdateEvent.AddListener((float distance) => spawnController.Distance = distance);
    }
}

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpawnController : MonoBehaviour, IUpdate {
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private Box boxPref;
    [SerializeField] private int maxCountDisableBox;

    public float Speed {
        set {
            speed = value;
            for (int i = 0; i < enableBoxes.Count; ++i) {
                enableBoxes[i].Speed = value;
            }
            for (int i = 0; i < disableBoxes.Count; ++i) {
                disableBoxes[i].Speed = value;
            }
        }
    }
    private float speed;

    public float Distance {
        set {
            distance = value;
            for (int i = 0; i < enableBoxes.Count; ++i) {
                enableBoxes[i].Distance = value;
            }
            for (int i = 0; i < disableBoxes.Count; ++i) {
                disableBoxes[i].Distance = value;
            }
        }
    }
    private float distance;
    
    private int CountBoxes => enableBoxes.Count + disableBoxes.Count;

    public float SpawnInterval {
        set {
            spawnInterval = value;
            timeToSpawn = 0;
        }
    }
    private float spawnInterval;

    private float timeToSpawn;
    
    private List<Box> enableBoxes = new List<Box>();
    private List<Box> disableBoxes = new List<Box>();

    [HideInInspector] public UnityEvent<Box> SpawnBoxEvent = new UnityEvent<Box>();
    [HideInInspector] public UnityEvent<Box> DestroyBoxEvent = new UnityEvent<Box>();

    public void MyFixedUpdate() {
        if (spawnInterval > 0) {
            timeToSpawn -= Time.fixedDeltaTime;
            if (timeToSpawn < 0) {
                timeToSpawn = spawnInterval;
                if (speed > 0 && distance > 0) {
                    Spawn();
                } else if (CountBoxes == 0) {
                    Spawn();
                }
            }
        }
    }

    private void Spawn() {
        Box box;
        if (disableBoxes.Count > 0) {
            box = disableBoxes[0];
            disableBoxes.RemoveAt(0);
        } else {
            box = Instantiate(boxPref, spawnPoint);
            box.DisableEvent.AddListener(DisableBox);
        }
        box.speed = speed;
        box.distance = distance;
        box.direction = new Vector3(Random.Range(-1f, 1f),
                                    Random.Range(-1f, 1f),
                                    Random.Range(-1f, 1f));
        box.transform.position = spawnPoint.position;
        box.gameObject.SetActive(true);
        enableBoxes.Add(box);
        SpawnBoxEvent.Invoke(box);
    }

    private void DisableBox(Box box) {
        enableBoxes.Remove(box);
        DestroyBoxEvent.Invoke(box);
        if (disableBoxes.Count < maxCountDisableBox) {
            disableBoxes.Add(box);
        } else {
            box.Destroy();
        }
    }
}

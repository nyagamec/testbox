using System.Collections.Generic;
using UnityEngine;

public class UpdateController : MonoBehaviour {
    [SerializeField] private List<IUpdate> updates = new List<IUpdate>();

    public void Add(IUpdate update) {
        updates.Add(update);
    }

    public void Remove(IUpdate update) {
        updates.Remove(update);
    }
    
    void FixedUpdate() {
        for(int i = 0; i < updates.Count; ++i) {
            updates[i].MyFixedUpdate();
        }
    }
}

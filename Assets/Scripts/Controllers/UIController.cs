using UnityEngine.Events;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour {
    [SerializeField] private TMP_InputField intervalIF;
    [SerializeField] private TMP_InputField speedIF;
    [SerializeField] private TMP_InputField distanceIF;

    [HideInInspector] public UnityEvent<float> IntervalUpdateEvent = new UnityEvent<float>();
    [HideInInspector] public UnityEvent<float> SpeedUpdateEvent = new UnityEvent<float>();
    [HideInInspector] public UnityEvent<float> DistanceUpdateEvent = new UnityEvent<float>();

    private void Awake() {
        intervalIF.onValueChanged.AddListener((string str) => ChangeValue(str, IntervalUpdateEvent));
        speedIF.onValueChanged.AddListener((string str) => ChangeValue(str, SpeedUpdateEvent));
        distanceIF.onValueChanged.AddListener((string str) => ChangeValue(str, DistanceUpdateEvent));
    }

    private void ChangeValue(string str, UnityEvent<float> e) {
        float res;
        if (float.TryParse(str, out res)) {
            e.Invoke(res);
        }
    }
}

using UnityEngine;
using UnityEngine.Events;

public class Box : MonoBehaviour, IUpdate {
    public float Speed { set => speed = value; }
    public float speed;
    public float Distance { set => distance = value; }
    public float distance;
    public Vector3 Direction { set => direction = value; }
    public Vector3 direction;
    
    private Vector3 startPosition;

    [HideInInspector] public UnityEvent<Box> DisableEvent = new UnityEvent<Box>();

    public void OnEnable() {
        startPosition = transform.position;
    }

    public void MyFixedUpdate() {
        if (Vector3.Distance(startPosition, transform.position) <= distance) {
            transform.Translate(direction * speed * Time.fixedDeltaTime);
        } else {
            gameObject.SetActive(false);
        }
    }

    public void OnDisable() {
        DisableEvent.Invoke(this);
    }

    public void Destroy() {
        Destroy(gameObject);
    }
}
